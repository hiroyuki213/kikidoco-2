class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :itunes_track_id

      t.timestamps
    end
  end
end
