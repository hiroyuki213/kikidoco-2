class CreateTrackLists < ActiveRecord::Migration
  def change
    create_table :track_lists do |t|
      t.integer :user_id
      t.integer :list_id
      t.integer :track_id

      t.timestamps
    end
  end
end
