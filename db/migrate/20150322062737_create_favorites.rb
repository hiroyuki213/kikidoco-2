class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.integer :user_id
      t.integer :resource_id
      t.string :resource_type

      t.timestamps
    end
  end
end
